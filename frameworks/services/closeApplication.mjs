import readline from 'readline';
import Emitter from 'events';
import fs from 'fs';
import {deleteTable} from './createTable.mjs';

export const closeApplication = () => {
    const rl = readline.createInterface({
       input: process.stdin,
       output: process.stdout
    });

    const stopApp = async () => {
        await deleteTable();
        fs.rmSync('./access.log');
        process.on('exit', () => {
            console.log('Application stopped');
        });
        process.exit(0);
    };

    const eventEmitter = new Emitter();

    eventEmitter.on('exit', async () => {
        await stopApp();
    });

    rl.on('line', input => {
       switch (input) {
           case 'stop':
               eventEmitter.emit('exit');
               break;
       }
    });
};
