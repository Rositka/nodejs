import {Users} from './createTable.mjs';

export const checkId = async (req, res, next) => {
    const { id } = req.params;
    const findUsersById = await Users.findByPk(id);

    if (findUsersById !== null) {
        next();
    } else {
        const allUsersId = await Users.findAll({
            attributes: ['id'],
        })
        res.status(401).send({
            message1: `No such id exists, you can choose one of this 'id'`,
            message2: allUsersId
        }).end();
    }
};
