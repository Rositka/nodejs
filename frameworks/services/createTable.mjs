import mysql from 'mysql2';
import Sequelize from 'sequelize';

const sequelize = new Sequelize('user', 'root', 'rosita442839978z', {
    host: 'localhost',
    dialect: 'mysql',
    logging: false
});

export const Users = sequelize.define('users', {
    user_id: Sequelize.STRING,
    login: Sequelize.STRING,
    password: Sequelize.STRING,
    role: Sequelize.STRING
});

export const createTable = async () => {
    await sequelize.sync();
};

export const deleteTable = async () => {
    await Users.drop();
    console.log(`Table 'users' dropped!`);
};

export const createTableRow = async (login, password, role) => {
    await Users.create({
        login: `${login}`,
        password: `${password}`,
        role: `${role}`
    });
};

export const changeField = async (value, login) => {
    await Users.update({user_id: `${value}`}, {
        where: {login}
    });
};

export const findUsers = async value => {
    return await Users.findOne({
        where: {user_id: `${value}`}
    });
}
