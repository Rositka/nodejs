import express from 'express';
import bodyParser from 'body-parser';
import readline from 'readline';
import path from 'path';
import fs from 'fs';
import createError from 'http-errors';
import cookieParser from 'cookie-parser';
import logger from 'morgan';
import morgan from 'morgan';
import authRouter from './routes/auth.mjs';
import usersRouter from './routes/users.mjs';
import {createTable, createTableRow} from './services/createTable.mjs';
import {closeApplication} from './services/closeApplication.mjs';

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

rl.question(`Start the application? Enter 'yes/no' `, async answer => {
  if (answer === 'yes') {
    console.log('Application started');
    console.log(`To exit the application, enter 'stop'`);
    rl.close();
  } else {
    rl.close();
  }
  await createTable();
  await createTableRow('admin123', '12321', 'admin');
  closeApplication();
});

const app = express();
const __dirname = path.resolve();
const accessLogStream = fs.createWriteStream(path.join(__dirname, 'access.log'), { flags: 'a' });

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'twig');

app.use(logger('dev'));
app.use(morgan('combined', { stream: accessLogStream }));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.json());

app.use('/auth', authRouter);
app.use('/users', usersRouter);

app.use((req, res, next) => {
  next(createError(404));
});

app.use((err, req, res, next) => {
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  res.status(err.status || 500);
  res.render('error');
});

export default app;
