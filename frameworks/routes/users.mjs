import express from 'express';
import {createTableRow, Users, findUsers} from '../services/createTable.mjs';
import {checkId} from '../services/middleware.mjs';
import fs from 'fs';

const checkAuthorization = async (req, res, next) => {
  try {
    const users = await findUsers(req.headers.token);

    if (users === null) {
      return res.status(401).send({
        message: 'Incorrect token',
      });
    }

    if (users.role !== 'admin') {
      return res.status(401).send({
        message: `You do not have permission to execute the command only 'admin'`
      });
    }

    next();
  } catch (error) {
    res.status(400).end();
  }
};

const router = express.Router();

router.use(checkAuthorization);

router.post('/', async (req, res, next) => {
  await createTableRow(req.body.login, req.body.password, req.body.role);

  res.status(200).send({
    message: 'You successfully create user'
  }).end();
});

router.get('/', async (req, res) => {
  const findAllUsers = await Users.findAll({
    attributes: ['id', 'login', 'role'],
  });

  res.status(200).send({
    message: findAllUsers,
  }).end();
});

router.delete('/:id', checkId, async (req, res) => {
  const { id } = req.params;
  const findUsersById = await Users.findByPk(id);

  await Users.destroy({
    where: {id: `${id}`}
  });

  const data = {
    id: findUsersById.id, login: findUsersById.login, role: findUsersById.role
  };
  res.status(200).send({
    message1: 'You deleted user',
    message2: data
  }).end();
});

router.get('/:id', checkId, async (req, res) => {
  const { id } = req.params;
  const findUsersById = await Users.findByPk(id);
  const data = {
    id: findUsersById.id, login: findUsersById.login, role: findUsersById.role
  };
  res.status(200).send({
    message: data,
  }).end();
});

router.patch('/:id', checkId, async (req, res) => {
  try {
    const { id } = req.params;
    const { login, password, role } = req.body;

    await Users.update({login: login, password: password, role: role}, {
      where: {id: `${id}`}
    });
    res.status(200).send({
      message1: 'You have made changes to the table',
    }).end();
  } catch (error) {
    res.status(400).end();
  }
});

router.get('/report', async (req, res) => {
  const findAllUsers = await Users.findAll( {
        attributes: ['id', 'login', 'role'],
      }
  );
  res.render('users', {
    title: 'Hello',
    findAllUsers
  }, (err, html) => {
    fs.writeFileSync('./index.html', html);
    res.download('./index.html');
  });
});

export default router;
