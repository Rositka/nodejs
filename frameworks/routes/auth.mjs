import express from 'express';
import {changeField, Users} from '../services/createTable.mjs';

const router = express.Router();

router.post('/', async (req, res, next) => {
    try {
        const users = await Users.findOne({
            where: {login: `${req.body.login}`}
        });

        if (!users) {
            return res.status(401).send({
                message: 'Incorrect login'
            });
        }

        if (req.body.password !== users.password) {
            return res.status(401).send({
                message: 'Incorrect password'
            });
        }

        const generateToken = Math.floor(Math.random() * 1000).toString();
        await changeField(generateToken, req.body.login);

        res.status(200).send({
            token: generateToken,
        }).end();
    } catch (error) {
        res.status(400).end();
    }
});

export default router;
