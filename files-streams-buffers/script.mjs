import http from 'http';
import readline from 'readline';
import Emitter from 'events';
import fs from 'fs';

let pathToDir;
let arrFiles;
let arrNewFiles;
let statOld;
let statNew;

const hostname = '127.0.0.1';
const port = 3000;

const server = http.createServer((req, res) => {
    const data = fs.createReadStream('./demo.json');
    res.setHeader('Context-Type', 'application/json');
    data.pipe(res);
});

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

fs.rmSync('./demo.json');

rl.question('Enter the path to directory ', path => {
    pathToDir = `${path}`;
    rl.close();
    readDir(pathToDir);
    statsOfFiles();
    closeApplication();
});

const readDir = dirName => {
    fs.readdir(dirName, (err, files) => {
        if (err) {
            console.log(err);
        } else {
            arrFiles = files;
            statOld = files.map(item => fs.statSync(`${pathToDir}/${item}`));
        }
    });
};

const readChangedDir = () => {
    fs.readdir(pathToDir, (err, files) => {
        if (err) {
            console.log(err);
        } else {
            arrNewFiles = files;
            statNew = files.map(item => fs.statSync(`${pathToDir}/${item}`));
        }
        if(arrFiles.toString() !== arrNewFiles.toString()) {
            const indexOfFilename = arrFiles.indexOf(arrFiles.filter((item, index) =>
                item !== arrNewFiles[index])[0]);
            const data = {
                oldFilename: arrFiles[indexOfFilename], newFilename: arrNewFiles[indexOfFilename]
            };
            console.log('File ' + arrFiles[indexOfFilename] + ' was renamed to the file ' + arrNewFiles[indexOfFilename]);
            writeContentToFile('File was renamed ', data);
            arrFiles = [...arrNewFiles];
        }
        const statisticOld = JSON.stringify(statOld);
        const statisticNew = JSON.stringify(statNew);
        if(statisticOld !== statisticNew) {
            const indexOfFile = statOld.indexOf(statOld.filter((item, index) =>
                JSON.stringify(item) !== JSON.stringify(statNew[index]))[0]);
            const data = {
                name: arrFiles[indexOfFile], sizeOld: statOld[indexOfFile].size, sizeNew: statNew[indexOfFile].size,
                mode: statNew[indexOfFile].mode, ctime: statNew[indexOfFile].ctime, atime: statNew[indexOfFile].atime
            };
            console.log('Changes in file:\n', data);
            console.log('The content was appended to "demo.json"');
            console.log('Enter "stop" to exit the application');
            writeContentToFile('Changes made to the file:', data);
            statOld = [...statNew];
        }
    });
};

const serverListen = () => {
    server.listen(port, hostname, () => {
        const textContent = JSON.stringify('Hello');
        fs.appendFile('./demo.json', `${textContent}` + ',' + '\r\n', err => {
            if (err) {
                throw err;
            }
        });
        console.log(`Server running at http://${hostname}:${port}/`);
        console.log('Make changes to the files or rename');
    });
};

const statsOfFiles = () => {
    setInterval(() => {
        readChangedDir();
    }, 1000);
   serverListen();
};

const writeContentToFile = (text, content) => {
    const data = JSON.stringify(content, null, 2);
    const textContent = JSON.stringify(text);
    fs.appendFile('./demo.json', `${textContent}` + ', ' + '\r\n' + `${data}\r\n`, err => {
        if (err) {
            throw err;
        }
    });
};

const closeApplication = () => {
    const rl = readline.createInterface({
        input: process.stdin,
        output: process.stdout
    });

    const stopApp = () => {
        process.on('exit', () => {
            console.log('Application stopped');
        });
        process.exit(1);
    };

    const eventEmitter = new Emitter();

    eventEmitter.on('exit', () => {
        stopApp();
    });

    rl.on('line', input => {
        switch (input) {
            case 'stop':
                eventEmitter.emit('exit');
                break;
        }
    });
};
