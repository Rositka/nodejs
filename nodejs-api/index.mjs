import readline from 'readline';
import Emitter from 'events';
import os from 'os';
import http from 'http';
import { spawn } from 'child_process';

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

const operatingSystem = () => {
    console.log('My OS name is: ', os.platform());
    console.log('Full processor information is: ', os.cpus());
    console.log('Free memory is: ', os.freemem());
    console.log('System on is: ', os.uptime());
}

const createRequest = () => {
    const parameters = {
        host: 'w3schools.com',
        port: 80,
        path: '/',
        method: 'GET'
    };

    const request = http.request(parameters, response => {
        response.on('data', data => {
            console.log(`${data}`);
        });
        response.on('end', () => {
            console.log('ok')
        });
    });
    request.end();
};

const runNewProcess = () => {
    const child = spawn('node', ['main.js']);
    child.stdout.on('data', data => {
        console.log(`New js file running: ${data}`);
    });
    child.stderr.on('data', data => {
        console.error(`Error: ${data}`);
    });
};

const stopApp = () => {
    process.on('exit', () => {
        console.log('Application stopped');
    });
    process.exit(1);
};

const eventEmitter = new Emitter();

eventEmitter.on('welcome', () => {
    console.log('Enter your command');
    console.log('Command "What is your OS info?" in response, information about the OS');
    console.log('Command "Make a request on w3schools" in response, url');
    console.log('Command "Run new js file" in response, start a new file');
    console.log('Command "Stop application" or "Stop", exit from the program');
});

eventEmitter.emit('welcome');

eventEmitter.on('os', () => {
    operatingSystem();
});

eventEmitter.on('url', () => {
    createRequest();
});

eventEmitter.on('runJs', () => {
    runNewProcess();
});

eventEmitter.once('exit', () => {
    stopApp();
});

const opSystem = rl.on('line', input => {
    switch (input) {
        case 'What is your OS info?':
            eventEmitter.emit('os');
            break;
        case 'Make a request on w3schools':
            eventEmitter.emit('url');
            break;
        case 'Run a new js file':
            eventEmitter.emit('runJs');
            break;
        case 'Stop application':
            eventEmitter.emit('exit');
            break;
        case 'Stop':
            process.exit(1);
    }
    return opSystem;
});


