import readline from 'readline';
import Emitter from 'events';
import mysql from 'mysql2';
import Sequelize from 'sequelize';

let Student;

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

const sequelize = new Sequelize('user', 'root', 'rosita442839978z', {
    host: 'localhost',
    dialect: 'mysql',
    logging: false
});

const createTable = async () => {
    Student = sequelize.define('students', {
        firstName: Sequelize.STRING,
        lastName: Sequelize.STRING,
        age: Sequelize.SMALLINT,
        hobby: Sequelize.STRING
    });
    await sequelize.sync();
    console.log('Table "students" created!');
};

const findById = async id => {
    const students = await Student.findByPk(id);
    if (students === null) {
        console.log('The field with this id does not exist.');
        await findAllId();
    }
    console.log(JSON.stringify(students));
};

const findAllId = async () => {
    const studentsId = await Student.findAll({
        attributes: ['id'],
    });
    console.log('You can choose one of these id ' + JSON.stringify(studentsId));
};

const createRows = async (firstName, lastName, age, hobby) => {
    const data = {firstName: `${firstName}`, lastName: `${lastName}`, age: `${age}`, hobby: `${hobby}`}
    await Student.create({
        firstName: `${firstName}`,
        lastName: `${lastName}`,
        age: `${age}`,
        hobby: `${hobby}`
    });
    console.log('Row added to the table!');
    console.log(JSON.stringify(data));
};

const changeField = async (firstName, lastName, age, hobby, id) => {
        await Student.update({
            firstName: `${firstName}`, lastName: `${lastName}`, age: `${age}`, hobby: `${hobby}`
        }, {
            where: {id: `${id}`}
        });
    console.log('You successfully updated the table row');
};

const insertNewRows = () => {
    rl.question('Enter your fistName ', firstName => {
        rl.question('Enter your lastName ', lastName => {
            rl.question('Enter your age ', age => {
                 rl.question('Enter your hobby ', async hobby => {
                     await createRows(firstName, lastName, age, hobby);
                });
            });
        });
    });
};

const retrieveSelectedRows = () => {
    rl.question('Enter id for selected row ', async id => {
       await findById(id);
    });
};

const changedFieldForStudent = id => {
    rl.question('Enter new value for the field fistName ', firstName => {
        rl.question('Enter new value for the field lastName ', lastName => {
            rl.question('Enter new value for the field age ', age => {
                rl.question('Enter new value for the field hobby ', async hobby => {
                    await changeField(firstName, lastName, age, hobby, id);
                });
            });
        });
    });
};

const updateRecord = () => {
    rl.question('Enter id for the row you want to change ', async id => {
        const students = await Student.findByPk(id);
        if (students === null) {
            console.log('The field with this id does not exist.');
            await findAllId();
        } else {
            changedFieldForStudent(id);
        }
    });
};

const deleteRecord = async () => {
    rl.question('Enter id for the row you want to delete ', id => {
        const deleteRow = async () => {
            const students = await Student.findByPk(id);
            if (students === null) {
                console.log('The field with this id does not exist.');
                await findAllId();
            } else if (students != null) {
                await Student.destroy({
                    where: {id: `${id}`}
                });
                console.log('The row with id '+ `${id}`+ ' was deleted.');
            }
        }
        deleteRow();
    });
};

const deleteTable = async () => {
   await Student.drop();
   console.log('Table "students" dropped!');
};

const stopApp = async () => {
    await deleteTable();
    process.on('exit', () => {
        console.log('Application stopped');
    });
    process.exit(1);
};

const startApp = () => {
    const eventEmitter = new Emitter();
    eventEmitter.on('welcome', () => {
        console.log('Enter your command');
        console.log('Command "INSERT", inserts new rows into an existing table');
        console.log('Command "SELECT", retrieve rows selected from tables');
        console.log('Command "UPDATE", update a record in a table');
        console.log('Command "DELETE", delete a record from a table');
        console.log('Command "Stop", exit from the program');
    });

    eventEmitter.on('insert', () => {
        insertNewRows();
    });

    eventEmitter.on('select', () => {
        retrieveSelectedRows();
    });

    eventEmitter.on('update', () => {
        updateRecord();
    });

    eventEmitter.on('delete', async () => {
        await deleteRecord();
    });

    eventEmitter.once('exit', async () => {
        await stopApp();
    });

    eventEmitter.emit('welcome');

    rl.on('line', input => {
        switch (input) {
            case 'INSERT':
                eventEmitter.emit('insert');
                break;
            case 'SELECT':
                eventEmitter.emit('select');
                break;
            case 'UPDATE':
                eventEmitter.emit('update');
                break;
            case 'DELETE':
                eventEmitter.emit('delete');
                break;
            case 'Stop' :
                eventEmitter.emit('exit');
                break;
        }
    });
}
startApp();
await createTable();
